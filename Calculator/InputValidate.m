//
//  InputValidate.m
//  Calculator
//
//  Created by Ravi Prakash on 07/10/16.
//  Copyright © 2016 Ravi Prakash. All rights reserved.
//

#import "InputValidate.h"
#import "ViewController.h"

@implementation InputValidate

+ (NSString *) validDotForScreenText:(NSString *) screenText; {
    // last char is oprator
    if ([InputValidate isLastCharOperator:screenText]) {
        return [ZERO stringByAppendingString:DOT];
    } else if([InputValidate isItSecondDot:screenText]) {
        return @"";
    } else {
        return DOT;
    }
}

+ (BOOL) isItSecondDot:(NSString *) screenText {
    NSInteger dotIndex = -1;
    NSInteger opeIndex = -1;
    for (NSInteger index=screenText.length - 1; index >= 0 ; index--) {
        NSString *subString = [NSString stringWithFormat:@"%c",[screenText characterAtIndex:index]];
        if([InputValidate isOperator:subString]) {
            opeIndex = index;
            break;
        } else if ([subString isEqualToString:DOT]) {
            dotIndex = index;
            break;
        }
    }
    if(dotIndex > opeIndex) {
        return YES;
    } else {
        return NO;
    }
}

+ (BOOL) isOperator:(NSString *) string {
    for(NSString *ope in [self getOperatorsArray]) {
        if([string isEqualToString:ope]) {
            return YES;
        }
    }
    return NO;
}

+ (BOOL) isLastCharOperator:(NSString *) screenText {
    for(NSString *ope in [self getOperatorsArray]) {
        if([screenText hasSuffix:ope]) {
            return YES;
        }
    }
    return NO;
}

+ (NSArray *) getOperatorsArray {
    return @[ADD, SUB, DEV, MUL, POW, MOD];
}

+ (BOOL) isDigit:(NSString *) string {
    NSCharacterSet *inverTedDigit = [[NSCharacterSet decimalDigitCharacterSet] invertedSet];
    if ([string rangeOfCharacterFromSet:inverTedDigit].location == NSNotFound) {
        return YES;
    }
    return NO;
}

@end
