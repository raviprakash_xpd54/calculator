//
//  Operation.h
//  Calculator
//
//  Created by Ravi Prakash on 07/10/16.
//  Copyright © 2016 Ravi Prakash. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Operation : NSObject
+(NSString *) evalueateExpression:(NSString *)input;
@end
