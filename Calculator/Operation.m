//
//  Operation.m
//  Calculator
//
//  Created by Ravi Prakash on 07/10/16.
//  Copyright © 2016 Ravi Prakash. All rights reserved.
//

#import "Operation.h"
#import "ViewController.h"
#import "InputValidate.h"
#define ERROR @"Error"
@implementation Operation

+(NSString *) evalueateExpression:(NSString *)input {
    input = [Operation cleanInput:input];
    NSArray *priorityOpe = @[POW, MUL, DEV, MOD];
    for (NSInteger i = 0; i < priorityOpe.count; i++) {
        input = [Operation evaluateExpression:input forOperator:priorityOpe[i]];
        // emidiate return for Error
        if ([input containsString:ERROR]) {
            return ERROR;
        }
    }
    return [Operation evalueAddAndSub:input];
}

+ (NSString *) evalueAddAndSub:(NSString *) input {
    NSExpression  *expression = [NSExpression expressionWithFormat:[NSString stringWithFormat:@"%@", input]];
    return [expression expressionValueWithObject:nil context:nil];
}

// evalueateExpression and return rest result
+ (NSString *) evaluateExpression:(NSString *)input forOperator:(NSString *)ope {
    NSArray *split = [input componentsSeparatedByString:ope];
    NSString *result = split[0];
    for (long i = 1; i < split.count; i++) {
        result = [Operation evaluate:result rightString:split[i] operator:ope];
    }
    return result;
}

// Evaluate string and return concatinated string.
+ (NSString *) evaluate:(NSString *)leftString rightString:(NSString *)rightString operator:(NSString *)ope {
    NSString *sufix = [Operation getSufixNumber:leftString];
    NSString *prefix = [Operation getPrefixNumber:rightString];
    // explict return if input have error with dev 0 or Error String
    if (([ope isEqualToString:DEV] && [prefix isEqualToString:ZERO]) || [leftString containsString:ERROR] || [rightString containsString:ERROR]) {
        return ERROR;
    }
    ope = [ope isEqualToString:POW] ? @"**" : ope;
    NSExpression *expression;
    if ([ope isEqualToString:MOD]) {
        expression = [NSExpression expressionForFunction:@"modulus:by:" arguments:@[[NSExpression expressionWithFormat:sufix], [NSExpression expressionWithFormat:prefix]]];
    } else {
        // make it double in numeric expression
        NSString *numericExpression = [NSString stringWithFormat:@"%@%@%@", sufix, ope, prefix];
        expression = [NSExpression expressionWithFormat:numericExpression];
    }
    NSNumber *result = [expression expressionValueWithObject:nil context:nil];
    return [NSString stringWithFormat:@"%@%@%@",[Operation removeSufix:leftString sufix:sufix], result, [Operation removePrefix:rightString prefix:prefix]];
}

// Remove given prefix from input
+(NSString *) removePrefix:(NSString *)input prefix:(NSString *)prefix {
    NSString *removedString = [input copy];
    if ([input hasPrefix:prefix])
        removedString = [input substringFromIndex:[prefix length]];
    return removedString;
}

// Remove given suffix from input
+(NSString *) removeSufix:(NSString *)input sufix:(NSString *)sufix {
    NSString *removedString = [input copy];
    if ([input hasSuffix:sufix])
        removedString = [input substringToIndex:input.length - sufix.length];
    return removedString;
}

// Get sufix number till there isn't any operator
+(NSString *) getSufixNumber:(NSString *) input {
    NSInteger index = 0;
    for (long i = input.length - 1; i >= 0; i--) {
        NSString *tempChar = [NSString stringWithFormat:@"%c", [input characterAtIndex:i]];
        // avoid negative number
        if([InputValidate isOperator:tempChar] && i != 0) {
            index = i + 1;
            break;
        }
    }
    return [input substringFromIndex:index];
}

// Get prefix number till there isn't any operator
+(NSString *) getPrefixNumber:(NSString *) input {
    NSInteger index = input.length;
    for (long i = 0; i < input.length; i++) {
        NSString *tempChar = [NSString stringWithFormat:@"%c", [input characterAtIndex:i]];
        // avoid negative number
        if ([InputValidate isOperator:tempChar] && i != 0) {
            index = i;
            break;
        }
    }
    return [input substringToIndex:index];
}

//clean input remove 0. -> 0 5- -> 5
+ (NSString *) cleanInput:(NSString *) input {
    for (long i = 0; i < input.length - 1; i++) {
        NSString *tempChar = [NSString stringWithFormat:@"%c", [input characterAtIndex:i]];
        NSString *nextChar = [NSString stringWithFormat:@"%c",[input characterAtIndex:i+1]];
        // do 0. -> to 0
        if ([tempChar isEqualToString:DOT] && [InputValidate isOperator:nextChar]) {
            input = [input stringByReplacingCharactersInRange:NSMakeRange(i, 1) withString:@""];
        }
    }

    NSString *lastChar = [NSString stringWithFormat:@"%c", [input characterAtIndex:input.length - 1]];
    if ([InputValidate isDigit:lastChar]) {
        return input;
    } else {
        input = [input stringByReplacingCharactersInRange:NSMakeRange(input.length - 1, 1) withString:@""];
    }
    return input;
}
@end
