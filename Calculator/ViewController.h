//
//  ViewController.h
//  Calculator
//
//  Created by Ravi Prakash on 05/10/16.
//  Copyright © 2016 Ravi Prakash. All rights reserved.
//

#import <UIKit/UIKit.h>
#define ZERO @"0"
#define DOT @"."
#define ADD @"+"
#define SUB @"-"
#define DEV @"/"
#define MUL @"*"
#define POW @"^"
#define MOD @"%"
@interface ViewController : UIViewController


@end

