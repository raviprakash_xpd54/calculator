//
//  ViewController.m
//  Calculator
//
//  Created by Ravi Prakash on 05/10/16.
//  Copyright © 2016 Ravi Prakash. All rights reserved.
//

#import "ViewController.h"
#import "InputValidate.h"
#import "Operation.h"
@interface ViewController ()
@property (nonatomic, strong) IBOutlet UILabel *screen;
@property (nonatomic, strong) IBOutlet UIButton *button0;
@property (nonatomic, strong) IBOutlet UIButton *button1;
@property (nonatomic, strong) IBOutlet UIButton *button2;
@property (nonatomic, strong) IBOutlet UIButton *button3;
@property (nonatomic, strong) IBOutlet UIButton *button4;
@property (nonatomic, strong) IBOutlet UIButton *button5;
@property (nonatomic, strong) IBOutlet UIButton *button6;
@property (nonatomic, strong) IBOutlet UIButton *button7;
@property (nonatomic, strong) IBOutlet UIButton *button8;
@property (nonatomic, strong) IBOutlet UIButton *button9;
@property (nonatomic, strong) IBOutlet UIButton *dotButton;
@property (nonatomic, strong) IBOutlet UIButton *ceButton;
@property (nonatomic, strong) IBOutlet UIButton *clButton;
@property (nonatomic, strong) IBOutlet UIButton *modButton;
@property (nonatomic, strong) IBOutlet UIButton *devButton;
@property (nonatomic, strong) IBOutlet UIButton *powButton;
@property (nonatomic, strong) IBOutlet UIButton *mulButton;
@property (nonatomic, strong) IBOutlet UIButton *addButton;
@property (nonatomic, strong) IBOutlet UIButton *subButton;
@property (nonatomic, strong) IBOutlet UIButton *eqlButton;
@property (nonatomic) BOOL equalClicked;
@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self initializeAllButtons];
    // Do any additional setup after loading the view, typically from a nib.
}

- (void) initializeAllButtons {
    NSArray *allButtons = @[self.button0,
                            self.button1,
                            self.button2,
                            self.button3,
                            self.button4,
                            self.button5,
                            self.button6,
                            self.button7,
                            self.button8,
                            self.button9,
                            self.dotButton,
                            self.ceButton,
                            self.clButton,
                            self.modButton,
                            self.devButton,
                            self.powButton,
                            self.mulButton,
                            self.addButton,
                            self.subButton,
                            self.eqlButton];
    [allButtons enumerateObjectsUsingBlock:^(UIButton *  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        obj.layer.borderColor = [UIColor grayColor].CGColor;
        obj.layer.borderWidth = 0.5;
    }];
}


- (IBAction) digitClicked:(UIButton *)sender {
    // clear if equal clicked and trying to press digit
    if (self.equalClicked) {
        self.screen.text = ZERO;
        self.equalClicked = NO;
    }
    NSString *digit = sender.currentTitle;
    NSString *screenText = self.screen.text;
    // If screen have 0 then don't let other zero getting append
    if ([screenText isEqualToString:ZERO]) {
        if ([digit isEqualToString:ZERO]) {
            self.screen.text = ZERO;
        } else if ([digit isEqualToString:DOT]) {
            // append it cause it's first dot after default zero
            self.screen.text = [screenText stringByAppendingString:[InputValidate validDotForScreenText:screenText]];
        } else {
            self.screen.text = digit;
        }
    } else {
        // if already contains DOT don't allow to enter one more.
        if ([digit isEqualToString:DOT]) {
            self.screen.text = [screenText stringByAppendingString:[InputValidate validDotForScreenText:screenText]];
        } else {
            self.screen.text = [screenText stringByAppendingString:digit];
        }
    }
}

- (IBAction) clearClicked:(id)sender {
    // Clear screen
    self.screen.text = ZERO;
    NSLog(@"Screen Cleared");
}

- (IBAction) clearEntry:(id)sender {
    // Don't remove last zero or if already have zero
    NSString *screenText = self.screen.text;
    if(![screenText isEqualToString:ZERO]) {
        self.screen.text = screenText.length > 1 ? [screenText substringToIndex:screenText.length - 1] : ZERO;
    }
}

- (IBAction)operator:(UIButton *)sender {
    // if operator get clicked
    self.equalClicked = NO;
    NSString *operator = sender.currentTitle;
    NSString *screenText = self.screen.text;
    if([operator isEqualToString:SUB]) {
        if ([screenText isEqualToString:ZERO]) {
            // if it's fist '-' then show it on screen
            self.screen.text = SUB;
        } else if(![InputValidate isLastCharOperator:screenText]){
            self.screen.text = [screenText stringByAppendingString:SUB];
        }
    } else if([InputValidate isOperator:operator] && ![InputValidate isLastCharOperator:screenText]) {
        self.screen.text = [screenText stringByAppendingString:operator];
    }
}

- (IBAction)equalClicked:(UIButton *)sender {
    self.screen.text = [NSString stringWithFormat:@"%@",[Operation evalueateExpression:self.screen.text]];
    self.equalClicked = YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
