//
//  InputValidate.h
//  Calculator
//
//  Created by Ravi Prakash on 07/10/16.
//  Copyright © 2016 Ravi Prakash. All rights reserved.
//

#import <Foundation/Foundation.h>
@interface InputValidate : NSObject
+ (NSString *) validDotForScreenText:(NSString *) screenText;
+ (BOOL) isOperator:(NSString *) string;
+ (BOOL) isDigit:(NSString *) string;
+ (BOOL) isLastCharOperator:(NSString *) screenText;
@end
